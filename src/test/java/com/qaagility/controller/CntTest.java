package com.qaagility.controller;

import static org.mockito.Mockito.*;
import org.junit.Test;
import static org.junit.Assert.*;


public class CntTest {

    @Test
	public void dWithZero() throws Exception {
	
	    assertEquals("TestWithZero",Integer.MAX_VALUE,new Cnt().d(1,0));
	}
	
	 @Test
	public void dWithNonZero() throws Exception {
	
	    assertEquals("TestWithNonZero",4,new Cnt().d(8,2));
	}
	
	 @Test
	public void dWithNegativeScenario() throws Exception {
	
	    assertNotEquals("TestWithNonZero",4,new Cnt().d(10,2));
	}

}